// credit: https://github.com/karpathy/recurrentjs/blob/84d69349c5412ddebd2b1cbc9b29861ddf3a8169/src/recurrent.js
// Utility fun
function assert(condition, message) {
  // from http://stackoverflow.com/questions/15313418/javascript-assert
  if (!condition) {
    message = message || "Assertion failed";
    if (typeof Error !== "undefined") {
      throw new Error(message);
    }
    throw message; // Fallback
  }
}

// Random numbers utils
let return_v = false;
let v_val = 0.0;
let gaussRandom = function () {
  if (return_v) {
    return_v = false;
    return v_val;
  }
  let u = 2 * Math.random() - 1;
  let v = 2 * Math.random() - 1;
  let r = u * u + v * v;
  if (r == 0 || r > 1) return gaussRandom();
  let c = Math.sqrt(-2 * Math.log(r) / r);
  v_val = v * c; // cache this
  return_v = true;
  return u * c;
}
let randf = function (a, b) { return Math.random() * (b - a) + a; }
let randi = function (a, b) { return Math.floor(Math.random() * (b - a) + a); }
let randn = function (mu, std) { return mu + gaussRandom() * std; }

// helper function returns array of zeros of length n
// and uses typed arrays if available
let zeros = function (n) {
  if (typeof (n) === 'undefined' || isNaN(n)) { return []; }
  if (typeof ArrayBuffer === 'undefined') {
    // lacking browser support
    let arr = new Array(n);
    for (let i = 0; i < n; i++) { arr[i] = 0; }
    return arr;
  } else {
    return new Float64Array(n);
  }
}

// Mat holds a matrix
let Mat = function (n, d) {
  // n is number of rows d is number of columns
  this.n = n;
  this.d = d;
  this.w = zeros(n * d);
  this.dw = zeros(n * d);
}
Mat.prototype = {
  get: function (row, col) {
    // slow but careful accessor function
    // we want row-major order
    let ix = (this.d * row) + col;
    assert(ix >= 0 && ix < this.w.length);
    return this.w[ix];
  },
  set: function (row, col, v) {
    // slow but careful accessor function
    let ix = (this.d * row) + col;
    assert(ix >= 0 && ix < this.w.length);
    this.w[ix] = v;
  },
  toJSON: function () {
    let json = {};
    json['n'] = this.n;
    json['d'] = this.d;
    json['w'] = this.w;
    return json;
  },
  fromJSON: function (json) {
    this.n = json.n;
    this.d = json.d;
    this.w = zeros(this.n * this.d);
    this.dw = zeros(this.n * this.d);
    for (let i = 0, n = this.n * this.d; i < n; i++) {
      this.w[i] = json.w[i]; // copy over weights
    }
  }
}

let softmax = function (m) {
  let out = new Mat(m.n, m.d); // probability volume
  let maxval = -999999;
  for (let i = 0, n = m.w.length; i < n; i++) { if (m.w[i] > maxval) maxval = m.w[i]; }

  let s = 0.0;
  for (let i = 0, n = m.w.length; i < n; i++) {
    out.w[i] = Math.exp(m.w[i] - maxval);
    s += out.w[i];
  }
  for (let i = 0, n = m.w.length; i < n; i++) { out.w[i] /= s; }

  // no backward pass here needed
  // since we will use the computed probabilities outside
  // to set gradients directly on m
  return out;
}


let sphere = {
  calculateVolume: function (radius) {
    return (4 / 3) * Math.PI * Math.pow(radius, 3);
  },
  calculateArea: function (radius) {
    return 4 * Math.PI * Math.pow(radius, 2);
  }
};


module.exports.softmax = softmax;
module.exports.sphere = sphere;