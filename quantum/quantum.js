let {softmax, sphere} = require('./library/maths.js');
let {clone} = require('./library/utilities.js');

let physics = (function () {
  let artificialCoulombsConstant = 0.5;

  let seek = function (parent, target) {
    return (parent.position - target.position).normalized();
  }

  let force = function (power) {
    let instantiate = function (radius, mass) {
      forceObject = {
        radius: radius,
        mass: mass,
        getVolume: sphere.calculateVolume.bind(radius),
        getArea: sphere.calculateArea.bind(radius),
        getDensity: function () {
          return mass / this.getVolume();
        }
      }  
      
      return calculate.bind(forceObject);
    }

    let calculate = function (parent, distance) {
      return (parent.mass * this.power) / Math.pow(distance, 2);
    }

    let apply = function (parent, target) {
      parent.update();

      parent.destroy();
      parent.find()
    }
    
    return {
      instantiate: instantiate,
      apply: apply
    };
  };

  let update = function (deltaTimeSeconds, frame) {
    for (let particle of frame.particles) {
      particle.update(deltaTimeSeconds);
      
    }

  }

  let step = function (forceModifier, seekBehaviour) {
    
  }

  let gravitational = force(1);
  let electric = force(artificialCoulombsConstant);

  return {
    force: force,
    seek: seek
  };
} ());

module.exports.physics = physics;