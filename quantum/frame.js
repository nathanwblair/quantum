let {softmax, sphere} = require('./library/maths.js');
let {clone} = require('./library/utilities.js');

let frame = function (particles) {

  let step = function (timestamp) {
    for (let particle of particles) {
      particle.step(timestamp);
     }
  }

  return {
    particles: particles,
    step: step
  }
}

let frames = (function () {
  var deltaFrameSeconds = 1 / 60;

  var frames = {};
  var timestamps = [];
  
  let add = function (timestamp, frame, broadcaster) {
    timestamps.push(timestamp);

    frames[timestamp] = frame;

    broadcaster.emit('send:frame', {
      timestamp: timestamp,
      frame: frame,
    });
  }

  let latest = function () {
    return frames[latestTimestamp()];
  }

  let latestTimestamp = function () {
    return timestamps[timestamps.length - 1];
  }

  let next = function (timestamp) {
    return frames[nextTimestamp(timestamp)];
  }

  let nextTimestamp = function (timestamp) {
    return timestamps[timestamps.indexOf(timestamp) + 1];
  }

  let update = function (startTimestamp, endTimestamp) {
    let startFrame = frames[startTimestamp];

    let result = clone(startFrame);
    let timestep = endTimestamp - startTimestamp;
    
    result.step(timestep);
  }

  return {
    add: add,
    next: next,
    nextTimestamp: nextTimestamp,
    latest: latest,
    latestTimestamp: latestTimestamp
  };
} ());


module.exports.frames = frames;