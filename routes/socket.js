var JustMath = require("justmath"),
  Vec2 = JustMath.Vec2;

// credit: http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object
function clone(obj) {
  var copy;

  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) return obj;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (var i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    }
    return copy;
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
}

let object = {
  key: function (n) {
    return this[Object.keys(this)[n]];
  }
};

function key(obj, idx) {
  return object.key.call(obj, idx);
}

let frames = (function () {
  var frames = {};
  var timestamps = [];

  let add = function (timestamp, frame, broadcaster) {
    timestamps.push(timestamp);

    frames[timestamp] = frame;

    broadcaster.emit('send:frame', {
      timestamp: timestamp,
      frame: frame
    });
  }

  let latest = function () {
    return frames[latestTimestamp()];

  }

  let latestTimestamp = function () {
    return timestamps[timestamps.length - 1];
  }

  let next = function (timestamp) {
    return frames[nextTimestamp(timestamp)];
  }

  let nextTimestamp = function (timestamp) {
    return timestamps[timestamps.indexOf(timestamp) + 1];
  }

  return {
    add: add,
    next: next,
    nextTimestamp: nextTimestamp,
    latest: latest,
    latestTimestamp: latestTimestamp
  };
} ());

module.exports.frames = frames;

let inputDirectionMap = {
  w: [0, 1.0],
  a: [-1.0, 0],
  s: [0, -1.0],
  d: [1.0, 0]
}

let add = function (a, b) {
  return (new Vec2(a[0], a[1])).add(new Vec2(b[0], b[1]));
}

let getRawVec2 = function (vec2) {
  return [vec2.x, vec2.y];
}

let toVec2 = function (array) {
  return new Vec2(array[0], array[1]);
}

module.exports.toVec2 = toVec2;
module.exports.getRawVec2 = getRawVec2;

// export function for listening to the socket
module.exports.socket = function (socket) {
  let particle = {
    position: [1, 1],
    color: "rgb(255, 0, 0)",
    direction: [0, 0],

    addDirection: function (value) {
      let newDirection = add(this.direction, value).norm();

      this.direction = getRawVec2(newDirection);
    }
  }; this
  // send the new client latest frame
  socket.emit('init', {
    frame: frames.latest(),
    timestamp: frames.latestTimestamp()
  });

  frames.latest().particles.push(particle);

  // clean up when a client leaves TODO
  socket.on('change:color', function (data) {
    frame = clone(frames.latest());
    frame.particles[0].color = data.color;
    //frames.add(frames.latestTimestamp() + 1, frame, socket.broadcast);
  });

  // clean up when a client leaves TODO
  socket.on('send:input', function (data) {
    frame = clone(frames.latest());

    if (data.timestamp === frames.latestTimestamp()) {
      particle.addDirection(inputDirectionMap[data.input]);
    }
  });

  // clean up when a client leaves TODO
  socket.on('send:color', function (data) {
    particle.color = data.color;
  });

  // clean up when a client leaves TODO
  socket.on('send:texture', function (data) {
    particle.texture = data.texture;
  });

  // clean up when a client leaves TODO
  socket.on('send:name', function (data) {
    particle.name = data.name;
  });


  // clean up when a client leaves TODO
  socket.on('disconnect', function () {
    //socket.broadcast.emit('client:left', {
    //  id: clientId
    //});
  });
};