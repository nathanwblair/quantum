'use strict';

/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');

var main = require('./routes/socket.js');
var socket = main.socket;
var frames = main.frames;
var toVec2 = main.toVec2;
var getRawVec2 = main.getRawVec2;

var app = express();
var server = http.createServer(app);

var port = process.env.PORT || 8080;

/* Configuration */
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));
app.set('port', 3000);

/* Socket.io Communication */
var io = require('socket.io').listen(server);
io.sockets.on('connection', socket);

/* Start server */
server.listen(port, function () {
  console.log('Express server listening @ http://localhost:%d in %s mode', port, app.get('env'));
});


var frame = {
  particles: [
    {
      position: [1, 1],
      color: "rgb(0, 255, 0)"
    },
    {
      position: [3, 1],
      color: "rgb(255, 255, 0)"
    },
    {
      position: [0, 100],
      color: "rgb(255, 0, 0)"
    }
  ]
};

frames.add(1, frame, io.sockets);


frame = {
  particles: [{
    position: [1, 1],
    color: "rgb(255, 0, 0)",
    direction: [0, 0],

    addDirection: function (value) {
      let newDirection = add(direction, value).norm();

      direction = getRawVec2(newDirection);
    }
  }]
};
frames.add(frames.nextTimestamp(), frame, io.sockets);

let deltaTime = 0.05;

function move() {
  for (let particle of frame.particles) {
    particle.position = getRawVec2(toVec2(particle.position).add(toVec2(particle.direction).scale(deltaTime)));
  }

  frames.add(frames.nextTimestamp(), frame, io.sockets);

  for (let particle of frame.particles) {
    particle.direction = [0, 0];
  }
}

setInterval(move, 100);

module.exports = app;
