import React from 'react';
import React3 from 'react-three-renderer';
import THREE from 'three';
import ReactDOM from 'react-dom';

let io = require('socket.io-client');
let socket = io.connect();
const width = window.innerWidth / 2; // canvas width
const height = window.innerHeight / 2; // canvas height
THREE.TextureLoader.prototype.crossOrigin = 'anonymous';

var widthHalf = width / 2, heightHalf = height / 2;

class Particle extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
		};

		this._onAnimate = () => {
		};
	}

	render() {
		return (
			<sprite
				name="particle"

				position={new THREE.Vector3(this.props.position[0], this.props.position[1], 0) }
				>
				<spriteMaterial
					url={this.props.texture}
					color={this.props.color}
					/>
			</sprite>
		);
	}
}

class ParticleBox extends React.Component {
	constructor(props, context) {
		super(props, context);

		// construct the position vector here, because if we use 'new' within render,
		// React will think that things have changed when they have not.
		this.cameraPosition = new THREE.Vector3(0, 0, 5);

		this.state = {
		};

		this._onAnimate = function () {
		};
	}

	render() {
		return (
			<div>
				<React3
					mainCamera="camera" // this points to the perspectiveCamera which has the name set to "camera" below
					width={this.props.width}
					height={this.props.height}

					onAnimate={this._onAnimate}
					>

					<scene>
						<perspectiveCamera
							name="camera"
							fov={75}
							aspect={this.props.width / this.props.height}
							near={0.1}
							far={1000}

							position={this.cameraPosition}
							/>
						{
							this.props.particles.map((particle, i) => {
								return (
									<Particle
										key={i}
										position={particle.position}
										color={particle.color}
										texture={particle.texture}
										/>
								);
							})
						}
					</scene>
				</React3>

				<div>
					{
						this.props.particles.map((particle, i) => {
							var camera = new THREE.PerspectiveCamera(75, this.props.width / this.props.height, 0.1, 1000);

							var scale = 40.0;

							var pos = new THREE.Vector3(particle.position[0], particle.position[1], 1);
							pos.x = pos.x - widthHalf * scale;
							pos.y = - pos.y + heightHalf * scale;
							

							return (
								<div style={{ position: 'absolute', 'z-index': 1, left: pos.x + 'px', top: pos.y + 'px', width: 100 + 'px', height: 20 + 'px', 'backgroundColor': 'rgb(119,136,153, 0.5)', 'opacity': 0.7, 'color': 'white' }}>
									{particle.name | ''}
								</div>
							);
						})
					}
				</div>
			</div>);
	}
}

class ParticleStream extends React.Component {
	constructor(props, context) {
		super(props, context);

		// construct the position vector here, because if we use 'new' within render,
		// React will think that things have changed when they have not.
		this.cameraPosition = new THREE.Vector3(0, 0, 5);

		this.state = {
			frames: null,
			color: "#c42e2e"
		};

		this._onAnimate = () => {
			// we will get this callback every frame

			// pretend cubeRotation is immutable.
			// this helps with updates and pure rendering.
			// React will be sure that the rotation has now updated.
			//	this.setState({
			//		particlePosition: new THREE.Vector3(0, this.state.particlePosition.y + 0.1, 0)
			//	});
		};
	}

	subscribe(keyevent, key) {
		$(document).on(keyevent, null, key, (this.emitInput).bind(this, key));
	}

	componentDidMount() {
		socket.on('init', this._initialize.bind(this));
		socket.on('send:frame', this._frameRecieve.bind(this));

		this.subscribe('keypress', 'w');
		this.subscribe('keypress', 'a');
		this.subscribe('keypress', 's');
		this.subscribe('keypress', 'd');
	}

	emitInput(input) {
		socket.emit('send:input', {
			'timestamp': this.state.timestamp,
			'input': input
		});
	}

	_initialize(data) {
		let {frame, timestamp} = data;
		frames = {};
		frames[timestamp] = frame;
		this.setState({ frames: frames, timestamp: timestamp });
	}

	_frameRecieve(data) {
		let {frames} = this.state;
		frames[data.timestamp] = data.frame;
		this.setState({ frames: frames, timestamp: data.timestamp });
	}

	getParticles() {
		if (this.state.frames) {
			let frame = this.state.frames[this.state.timestamp];

			if (frame)
				return frame.particles;
		}

		return [];
	}

	 onColorChange(e) {
		this.setState({ color: e.target.value });
		console.log([e.target.value, this.state.color]);
		socket.emit('send:color', {
			'color': e.target.value
		});
	}

	 onImageChange(e) {
		 this.setState({ texture: e.target.value });
		 console.log([e.target.value, this.state.texture]);
		 socket.emit('send:texture', {
			 'texture': e.target.value
		 });
	 } 

	 onNameChange(e) {
		this.setState({ name: e.target.value });
		console.log([e.target.value, this.state.name]);
		socket.emit('send:name', {
			'name': e.target.value
		});
	}

	render() {

		return (
			<div>
				<ParticleBox
					particles={this.getParticles.bind(this)() }
					width={width}
					height={height}
					/>

				<input
					type="url"
					onChange={this.onImageChange.bind(this) }
					value={this.state.texture}
					/>
				<input
					type="text"
					onChange={this.onNameChange.bind(this) }
					value={this.state.name}
					/>
				<input
					type="color"
					id="html5colorpicker"
					onChange={this.onColorChange.bind(this) }
					value={this.state.color}
					/>
			</div>
		);
	}
}

ReactDOM.render(<ParticleStream/>, document.body);