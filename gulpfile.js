let gulp = require('gulp');
let browserify = require('browserify');
let babelify = require('babelify');
let source = require('vinyl-source-stream');
 
gulp.task('watch', function() {
    gulp.watch('**/*.jsx', ['build']);
});
 
gulp.task('build', function () {
  console.log("building");
  browserify({
    entries: 'client/app.jsx',
    extensions: ['.jsx'],
    debug: true
  })
  .transform(babelify)
  .bundle()
  .pipe(source('app.js'))
  .pipe(gulp.dest('public/scripts'));
});